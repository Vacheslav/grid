const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'production', // Режим сборки: "production" | "development" | "none"
  entry: {
    index: './src/index.js', // Входная точка приложения
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'build'),
    library: 'PackLibrary',
    libraryTarget: 'umd'
  },
  devtool: 'source-map', 
  devServer: {
    static: {
      directory: path.join(__dirname, 'build'),
    },
  },
  devServer: {
    open: true,
    watchFiles: ['src/**/*'],
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: "raw-loader"
      },
      {
        test: /\.css$/i,
        use: [
          "style-loader",
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                  [
                    "postcss-preset-env",
                    {
                      // Options
                    },
                  ],
                ],
              },
            },
      },]},
      {
        test: /\.js$/, // Загрузчик JS
        exclude: /node_modules|bower_components/,
        use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              plugins: [
                [
                  "@babel/plugin-transform-runtime",
                  {
                    "corejs": 3
                  },
                  "@babel/plugin-transform-strict-mode"
                ]
              ]
            },
        }
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'My App', // Заголовок HTML страницы
      template: './src/index.html', // HTML шаблон
    }),
  ],
};